(function ($) {

  'use strict';

  $('.tp-calculator').each(function () {
    const transitions = $(this).find('.tp-calculator__transition');
    let animateTransition;
    $(this).hover(function () {
      const currentCalculator = $(this);
      let time = 0;
      transitions.each(function () {
        animateTransition = setTimeout(function (transition, calculator) {
          if (calculator.is(':hover')) {
            transition.removeClass('is-not-active');
          }
        }, time, $(this), currentCalculator);
        time += 600;
      });
    }, function () {
      transitions.addClass('is-not-active');
      clearTimeout(animateTransition);
    });
  });

})(jQuery);