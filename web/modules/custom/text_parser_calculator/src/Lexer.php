<?php

namespace Drupal\text_parser_calculator;

/**
 * Class Lexer.
 *
 * @package Drupal\text_parser_calculator
 */
class Lexer {

  const GROUP_MATCH = '/([0-9]*\.?[0-9]+)|([\+,\-,\/,\*]+?)/';
  // Check if text has only numbers, floats and +,-,*,/.
  const VALIDATE_MATCH = '/^(([0-9]*\.?[0-9]+)+([\+,\-,\*,\/]+=*[0-9]+)*)+$/';

  /**
   * Creates operators and operands tokens.
   *
   * @param string $text
   *
   * @return array
   */
  public function getTokens(string $text) {
    preg_match_all(self::GROUP_MATCH, $text, $tokens, PREG_UNMATCHED_AS_NULL);

    return !empty($tokens[0]) ? $tokens : [];
  }

  /**
   * Checks if the text is valid for token creation.
   *
   * @param string $text
   *
   * @return false|int
   */
  public function textIsValid(string $text) {
    return preg_match(self::VALIDATE_MATCH, $text);
  }

}
