<?php

namespace Drupal\text_parser_calculator\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\text_parser_calculator\Plugin\Field\TextParserCalculatorFormatterBase;

/**
 * Plugin implementation of the 'text_parser_calculator' formatter.
 *
 * @FieldFormatter(
 *   id = "text_parser_calculator",
 *   label = @Translation("Text parser calculator"),
 *   field_types = {
 *     "string",
 *   }
 * )
 */
class TextParserCalculatorFormatter extends TextParserCalculatorFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = ['#markup' => $this->viewValue($item)];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // Remove whitespaces and replace commas with dots.
    $text = str_replace(' ', '', str_replace(',', '.', $item->value));
    $theme = [
      '#theme' => 'text_parser_calculator',
      '#infix' => $text,
    ];
    if ($this->lexer->textIsValid($text)) {
      $tokens = $this->lexer->getTokens($text);
      if (!empty($tokens)) {
        $this->parser->setTokens($tokens);
        try {
          $theme['#postfix_transitions'] = $this->parser->getPostfixTransitions();
        }
        catch (\Exception $e) {
          $theme['#infix'] = t('Division with zero!');
        }
      }
    }
    else {
      $theme['#infix'] = t('Malformed input!');
    }

    return \Drupal::service('renderer')->render($theme);
  }

}
