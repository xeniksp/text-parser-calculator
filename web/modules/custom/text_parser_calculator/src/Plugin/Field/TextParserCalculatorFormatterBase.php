<?php

namespace Drupal\text_parser_calculator\Plugin\Field;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\text_parser_calculator\Lexer;
use Drupal\text_parser_calculator\Parser;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TextParserCalculatorFormatterBase.
 *
 * @package Drupal\text_parser_calculator\Plugin\Field
 */
abstract class TextParserCalculatorFormatterBase extends FormatterBase implements ContainerFactoryPluginInterface {

  protected $lexer;
  protected $parser;

  /**
   * {@inheritdoc} Store the new services.
   *
   * @param \Drupal\text_parser_calculator\Lexer $lexer
   * @param \Drupal\text_parser_calculator\Parser $parser
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, Lexer $lexer, Parser $parser) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->lexer = $lexer;
    $this->parser = $parser;
  }

  /**
   * {@inheritdoc} Inject the 'text_parser_calculator.lexer' and
   * text_parser_calculator.parser services to the Formatter.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      // text_parser_calculator services (Lexer and Parser).
      $container->get('text_parser_calculator.lexer'),
      $container->get('text_parser_calculator.parser')
    );
  }
}
