<?php

namespace Drupal\text_parser_calculator;

/**
 * Class Parser.
 *
 * @package Drupal\text_parser_calculator
 */
class Parser {

  const OPERATORS = [
    '+' => 2,
    '-' => 2,
    '*' => 3,
    '/' => 3,
  ];

  private $tokens;

  /**
   * Set the tokens.
   *
   * @param array $tokens
   *
   * @return $this
   */
  public function setTokens(array $tokens) {
    $this->tokens = $tokens;

    return $this;
  }

  /**
   * Convert the infix syntax to postfix syntax.
   *
   * @return array
   */
  private function infixToPostfix() {
    $postfix_tree = [];
    $operators_stack = [];
    foreach ($this->tokens[0] as $key => $token) {
      if (!is_null($this->tokens[1][$key])) {
        $postfix_tree[] = $token;
      }
      elseif (!is_null($this->tokens[2][$key])) {
        if (empty($operators_stack)) {
          $operators_stack[] = $token;
        }
        else {
          if (self::OPERATORS[$token] <= self::OPERATORS[array_values(
              $operators_stack
            )[0]]) {
            $postfix_tree[] = array_shift($operators_stack);
            if (empty($operators_stack)) {
              $operators_stack[] = $token;
            }
            else {
              foreach ($operators_stack as $operators_stack_key => $operators_stack_value) {
                if (self::OPERATORS[$token] <= self::OPERATORS[$operators_stack_value]) {
                  $postfix_tree[] = $operators_stack_value;
                  unset($operators_stack[$operators_stack_key]);
                  array_push($operators_stack, $token);
                }
                else {
                  array_unshift($operators_stack, $token);
                }
              }
            }
          }
          else {
            array_unshift($operators_stack, $token);
          }
        }
      }
      else {
        return [];
      }
    }
    if (!empty($operators_stack)) {
      $postfix_tree = array_merge($postfix_tree, $operators_stack);
    }

    return $postfix_tree;
  }

  /**
   * Returns the postfix tree.
   *
   * @return array
   */
  public function getPostfixTree() {
    return $this->infixToPostfix();
  }

  /**
   * Returns the postfix transitions.
   *
   * @return array
   *
   * @throws \Exception
   */
  public function getPostfixTransitions() {

    return $this->generatePostfixTransitions();
  }

  /**
   * Generate postfix transitions until the final result.
   *
   * @return array
   *
   * @throws \Exception
   */
  private function generatePostfixTransitions() {
    $postfix_tree = $this->infixToPostfix();
    $postfix_transitions = [];
    $postfix_transitions[] = $postfix_tree;
    $numeric_stack = [];
    foreach ($this->infixToPostfix() as $key => $op) {
      if (is_numeric($op)) {
        $numeric_stack[] = $op;
        unset($postfix_tree[$key]);
        continue;
      }
      switch ($op) {
        case '+':
          array_push(
            $numeric_stack,
            array_pop($numeric_stack) + array_pop($numeric_stack)
          );
          unset($postfix_tree[$key]);
          $postfix_transitions[] = $numeric_stack + $postfix_tree;
          break;

        case '-':
          $number = array_pop($numeric_stack);
          array_push($numeric_stack, array_pop($numeric_stack) - $number);
          unset($postfix_tree[$key]);
          $postfix_transitions[] = $numeric_stack + $postfix_tree;
          break;

        case '*':
          array_push(
            $numeric_stack,
            array_pop($numeric_stack) * array_pop($numeric_stack)
          );
          unset($postfix_tree[$key]);
          $postfix_transitions[] = $numeric_stack + $postfix_tree;
          break;

        case '/':
          $number = array_pop($numeric_stack);
          if ((float) $number === 0.00) {
            throw new \Exception('Division with zero!');
          }
          array_push($numeric_stack, array_pop($numeric_stack) / $number);
          unset($postfix_tree[$key]);
          $postfix_transitions[] = $numeric_stack + $postfix_tree;

          break;
      }
    }

    return $postfix_transitions;
  }

}
