<?php

use Drupal\Tests\UnitTestCase;
use Drupal\text_parser_calculator\Lexer;
use Drupal\text_parser_calculator\Parser;

/**
 * Class TextParserCalculatorTestCase.
 *
 * @coversDefaultClass \Drupal\text_parser_calculator\Parser
 * @group text_parser_calculator
 */
class TextParserCalculatorTestCase extends UnitTestCase {

  private $lexer;
  private $parser;

  /**
   * {@inheritdoc} Bring the Lexer and Parser classes.
   */
  protected function setUp() {
    parent::setUp();

    $this->lexer = new Lexer();
    $this->parser = new Parser();
  }

  /**
   * @covers ::infixToPostfix
   * @dataProvider infixToPostfixDataProvider
   */
  public function testInfixToPostfix($infix, $postfix) {
    $tokens = $this->lexer->getTokens($infix);
    $this->parser->setTokens($tokens);
    $actual_result = implode(' ', $this->parser->getPostfixTree());
    $this->assertEquals($postfix, $actual_result);
  }

  /**
   * Data provider for testInfixToPostfix().
   * Expected values are calculated here:
   * http://scanftree.com/Data_Structure/prefix-postfix-infix-online-converter.
   *
   * @return array
   *   Nested arrays of values to check:
   *   - $infix
   *   - $postfix
   */
  public function infixToPostfixDataProvider() {
    return [
      ['1+1-1*2+0', '1 1 + 1 2 * - 0 +'],
      ['100/9/9.99+1+2+3*0.25+65*1', '100 9 / 9.99 / 1 + 2 + 3 0.25 * + 65 1 * +'],
      ['74/1.25+6+0.1+96*8-8-7*5', '74 1.25 / 6 + 0.1 + 96 8 * + 8 - 7 5 * -'],
      ['0.2+0.3+0.4/99*8/0.5+6+6*1.12', '0.2 0.3 + 0.4 99 / 8 * 0.5 / + 6 + 6 1.12 * +'],
    ];
  }

  /**
   * @covers ::getPostfixTransitions
   * @dataProvider postfixResultDataProvider
   */
  public function testPostfixResult($infix, $math_result) {
    $tokens = $this->lexer->getTokens($infix);
    $this->parser->setTokens($tokens);
    $transitions = $this->parser->getPostfixTransitions();
    $actual_result = array_pop($transitions)[0];
    $this->assertEquals($math_result, $actual_result);
  }

  /**
   * Data provider for testPostfixResult().
   *
   * @return array
   *   Nested arrays of values to check:
   *   - $infix
   *   - $math_result
   */
  public function postfixResultDataProvider() {
    return [
      ['1+1-1*2+0', '0'],
      ['100/9/9.99+1+2+3*0.25+65*1', '69.862223334446'],
      ['74/1.25+6+0.1+96*8-8-7*5', '790.3'],
      ['0.2+0.3+0.4/99*8/0.5+6+6*1.12', '13.284646464646'],
    ];
  }

  /**
   * Test for handling exceptions.
   *
   * @covers ::getPostfixTransitions
   * @dataProvider postfixResultExceptionDataProvider
   */
  public function testPostfixResultException($infix) {
    $this->expectException(Exception::class);
    $tokens = $this->lexer->getTokens($infix);
    $this->parser->setTokens($tokens);
    $this->parser->getPostfixTransitions();
  }

  /**
   * Data provider for testPostfixResultException().
   *
   * @return array
   *   Nested arrays of values to check:
   *   - $infix
   */
  public function postfixResultExceptionDataProvider() {
    return [
      ['1/0'],
      ['1/0*0'],
      ['1/1/0.000'],
    ];
  }

}
