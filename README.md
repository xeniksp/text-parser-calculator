# Text parser calculator

## Site installation

1. Git clone the current repository and enter inside the folder.
1. Run `composer install`.
2. Create an empty database and virtual host.
3. Visit the site.
4. In "Choose profile" step, select `Use existing configuration`.
5. Finish the following steps.

## Text Parser Calculator

For field of type `Text (plain)`, there is a new format option available
in Manage display tab with name `Text parser calculator`.

Converts infix syntax to postfix syntax and shows all the steps until the 
final result.

> Allowed values are numbers, floats and +,-,*,/.

### Running tests

Enter `/web/core` directory and run `../../vendor/bin/phpunit --group=text_parser_calculator`.